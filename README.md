# Mnemosyne VS Code extension

Extension for using Mnemosyne with VS Code.

This is forked from [ailisp/commonlisp-vscode](https://github.com/ailisp/commonlisp-vscode) to provide basic support for editing Common Lisp code.

## Release

To build a new release:

``` shell
npm install -g vsce
make release # Build, tag, and push the new version.
```

Since GitLab support releases but doesn’t support uploading binaries, we store the binaries in the `release` branch of this repository.

Once the release has been uploaded, manually update the [releases page](https://gitlab.com/GrammaTech/Mnemosyne/mnemosyne-vscode/-/releases) with the new release.
