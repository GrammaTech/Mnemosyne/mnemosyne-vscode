.PHONY: clean

version := $(shell jq -r '.version' package.json)

all: mnemosyne-$(version).vsix

json_files := $(wildcard *.json)
ts_files := $(wildcard src/*.ts)

mnemosyne-$(version).vsix: $(json_files) $(ts_files)
	npm install
	vsce package

release: mnemosyne-$(version).vsix
	git checkout release
	git add mnemosyne-$(version).vsix
	git commit -m "Release $(version)"
	git push origin release

clean:
	rm -f *.vsix
	rm -rf out/*
