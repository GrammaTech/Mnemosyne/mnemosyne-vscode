import * as vscode from 'vscode';
import * as child_process from 'child_process';
import * as net from 'net';
import * as util from 'util';
import {
    workspace, Disposable, ExtensionContext, languages,
    window, commands, InputBoxOptions
} from 'vscode';
import { MessageType } from 'vscode-languageserver-protocol';
import {
    LanguageClient, LanguageClientOptions, ServerOptions,
    TextDocumentIdentifier, TextDocumentPositionParams,
    Position
} from 'vscode-languageclient/node';
import {inputCommitMessage} from './messages'

import {promises as fs} from 'fs'
import * as url from 'url'
import * as path from 'path'

let languageClient: LanguageClient;

function getTextDocumentIdentifier() {
    if (vscode.window.activeTextEditor) {
        const document = vscode.window.activeTextEditor.document;
        const params: TextDocumentIdentifier = {
            uri: document.uri.toString()
        }
        return params;
    } else {
        return null;
    }
}

function compileAndLoadFile() {
    const params = getTextDocumentIdentifier();
    if (params) {
        languageClient.sendNotification("lisp/compileAndLoadFile", params);
    } else {
        vscode.window.showErrorMessage('Need to open a file to compile and load');
    }
}

function evaluate() {
    if (vscode.window.activeTextEditor) {
        const selection = vscode.window.activeTextEditor.selection;
        if (selection.isEmpty) {
            const params: TextDocumentPositionParams = {
                textDocument: getTextDocumentIdentifier()!,
                position: selection.active
            }
            languageClient.sendNotification("lisp/eval", params);
        } else {
            const params = {
                textDocument: getTextDocumentIdentifier(),
                range: selection
            }
            languageClient.sendNotification("lisp/rangeEval", params);
        }
    } else {
        vscode.window.showErrorMessage("Need an editor window to evaluate");
    }
}

function interrupt() {
    languageClient.sendNotification("lisp/interrupt", {});
}

async function newlineAndFormat() {
    const editor = vscode.window.activeTextEditor!;
    const document = editor.document;
    let position = editor.selection.active;
    await editor.edit(e => e.insert(
        position,
        "\n"));
    position = editor.selection.active;
    const edits = await languageClient.sendRequest("textDocument/onTypeFormatting", {
        textDocument: { "uri": "file://" + document.uri.fsPath },
        position,
        ch: "\n",
        options: {
            tabSize: editor.options.tabSize,
            insertSpaces: editor.options.insertSpaces,
        }
    });

    const workEdits = new vscode.WorkspaceEdit();
    workEdits.set(document.uri, edits as vscode.TextEdit[]); // give the edits
    vscode.workspace.applyEdit(workEdits); // apply the edits
}

function retry<T>(retries: number, fn: () => Promise<T>): Promise<T> {
    return fn().catch((err) => retries > 1 ? retry(retries - 1, fn) : Promise.reject(err));
}
const pause = (duration: number) => new Promise(res => setTimeout(res, duration));
function backoff<T>(retries: number, fn: () => Promise<T>, delay = 500): Promise<T> {
    return fn().catch(err => retries > 1
        ? pause(delay).then(() => backoff(retries - 1, fn, delay * 2))
        : Promise.reject(err));
}

let ctx: ExtensionContext

async function excludePattern () {
    function concat_keys(obj: any) {
        obj = obj || {}
        return Object.keys(obj).join(",")
    }

    async function get_conf(x: string, y: string) {
        return workspace.getConfiguration(x, null).get(y)
    }

    return "{"
        + concat_keys(await get_conf('search', 'exclude'))
        + concat_keys(await get_conf('files', 'exclude'))
        + "}"
}

async function gotoPosition(uri: vscode.Uri, position: Position) {
    const {line, character} = position;
    const vs_pos = new vscode.Position(line, character);
    const vs_range = new vscode.Range(vs_pos, vs_pos);

    const doc = await workspace.openTextDocument(uri);
    await window.showTextDocument(doc);
    const editor = window.activeTextEditor;
    if (editor) {
        editor.selection = new vscode.Selection(vs_range.start, vs_range.end);
        editor.revealRange(vs_range, vscode.TextEditorRevealType.AtTop);
    }
}

function mnemosyneConnect () {
    const context = ctx;
    const serverOptions: ServerOptions = async () => {
        const client = new net.Socket();
        return await backoff(5, () => {
            return new Promise((resolve, reject) => {
                const port = workspace.getConfiguration().get<number>('mnemosyne.server.port');
                const host = workspace.getConfiguration().get<string>('mnemosyne.server.host');
                setTimeout(() => {
                    reject(new Error("Connection timed out. Is Mnemosyne running?"))
                }, 1000);
                client.connect(port!, host!);
                client.once('connect', () => {
                    resolve({
                        reader: client,
                        writer: client,
                    });
                });
            })
        });
    };

    const clientOptions: LanguageClientOptions = {
        documentSelector: ['commonlisp', 'python', 'c', 'cpp',
                           { scheme: 'file', language: 'javascript'},
                           { scheme: 'file', language: 'typescript'},
                           { scheme: 'file', language: 'typescriptreact'}
                          ],
        // TODO Can't make this work without incompatible library versions.
        // revealOutputChannelOn: "never",
        synchronize: {
            configurationSection: 'commonlisp'
        },
        initializationOptions: {
            argot: {
                disable: (workspace.getConfiguration().get<string>('mnemosyne.server.disable') || "")
                             .split(',')
                             .map(x => x.trim()),
                filesProvider: true,
                contentProvider: true,
                inputBoxProvider: true,
                gotoProvider: true
            }
        }
    }

    languageClient = new LanguageClient(
        "mnemosyne", // ID for configuration
        "Mnemosyne", // Name for display
        serverOptions,
        clientOptions
    );

    const assocs: any = workspace.getConfiguration("files").get("associations");
    if (!assocs["*.cppm"]) {
        assocs["*.cppm"] = "cpp";
        workspace.getConfiguration("files").update("associations", assocs, vscode.ConfigurationTarget.Workspace);
    }

    languageClient.onReady().then(function (x: any) {
        languageClient.onNotification("lisp/evalBegin", function (f: any) {
            window.setStatusBarMessage("Eval...");
        })
        languageClient.onNotification("lisp/evalEnd", function (f: any) {
            window.setStatusBarMessage("Done");
        })
        languageClient.onRequest("argot/window/goto", async (params: any) => {
            const {uri, position} = params;
            await gotoPosition(vscode.Uri.parse(uri), position);
        })
        languageClient.onRequest("argot/window/showPrompt", async (params: any) => {
            const args = {
                    prompt: params.message,
                    value:  params.defaultValue
                }
            const title = params.multiLine
                ? await inputCommitMessage(args)
                : await window.showInputBox(args);
            return {
                "title": title
            };
        })
        // Override showMessageRequest to use quickpick UI.
        languageClient.onRequest("window/showMessageRequest", async (params: any) => {
            const actions = params.actions || [];
            const items = actions.map((x: any) => x.title)
            const options = {
                placeHolder: params.message,
                canPickMany: false
            }
            const title = await window.showQuickPick(items, options);
            return {
                "title": title
            };
        })
        languageClient.onRequest("argot/textDocument/content", async (params: any) => {
            const doc = params.textDocument
            let p = url.fileURLToPath(new url.URL(doc.uri))
            if (vscode.workspace.rootPath) {
                p = path.resolve(vscode.workspace.rootPath, p)
            }
            // TODO This will break for unusual encodings. Also: is a
            // race condition possible here, where the file has been
            // opened and edited on the VS Code side but no didOpen
            // notification has been sent?
            const buf = await fs.readFile(p);
            if (buf.includes('\0')) {
                throw new Error('Binary file!');
            }
            const text = buf.toString('utf-8');
            return {...doc, languageId: '', version: 0, text: text}
        })
        languageClient.onRequest("argot/workspace/files", async (params: any): Promise<TextDocumentIdentifier[]> => {
            const folder = vscode.workspace.workspaceFolders?.[0];
            let files = [];
            if (folder) {
                const base = params.base || "";
                const excludes = await excludePattern();
                const pattern = '**/*.*';
                files = await vscode.workspace.findFiles(
                    new vscode.RelativePattern(folder, pattern),
                    excludes
                );
                files = files.map(u => ({ uri: u.toString()}));
                if (base) {
                    files = files.filter(f => f.uri.indexOf(base) == 0);
                }
                return files;
            } else {
                return [{ uri: 'No folder'}];
            }
        })
        languageClient.onRequest("argot/forwardClientRequest", async (params: any) => {
            const messageName = params.methodRequest;
            let messageParams, uri, position, result;

            // The following executed commands likely don't take arguments.
            // Currently, the cursor position isn't used by any mnemosyne muses,
            // so this doesn't matter. In the future, it may make sense to move
            // the cursor before calling the command to gatekeep which providers
            // provide something.
            switch(messageName) {
                case "textDocument/definition":
                    messageParams = params.clientRequestParams.definitionParams;
                    uri = vscode.Uri.parse(messageParams.textDocument.uri);
                    position = messageParams.position;
                    result = await commands.executeCommand(
                        'editor.action.revealDefinition',
                        uri,
                        position
                    );
                    break;
                case "textDocument/references":
                    messageParams = params.clientRequestParams.referenceParams;
                    uri = vscode.Uri.parse(messageParams.textDocument.uri);
                    position = messageParams.position;
                    result = await commands.executeCommand(
                        'references-view.findReferences',
                        uri,
                        position
                    );
                    break;
                    /*
                case "textDocument/documentHighlight":
                    messageParams = params.clientRequestParams.documentHighlightParams;
                    uri = vscode.Uri.parse(messageParams.textDocument.uri);
                    position = messageParams.position;
                    result = await commands.executeCommand(
                        // This is incorrect.
                        'editor.action.selectHighlights',
                        uri,
                        position
                    );
                    break;
                    */
            }

            return {
                "result" : result
            };
        })
    })

    context.subscriptions.push(languageClient.start());
}

export async function activate(context: ExtensionContext) {
    ctx = context;
    context.subscriptions.push(commands.registerCommand("mnemosyne.connect", mnemosyneConnect));
    context.subscriptions.push(commands.registerCommand("lisp.compileAndLoadFile", () => compileAndLoadFile()));
    context.subscriptions.push(commands.registerCommand("lisp.eval", () => evaluate()));
    context.subscriptions.push(commands.registerCommand("lisp.interrupt", () => interrupt()));
    context.subscriptions.push(commands.registerCommand("lisp.newlineAndFormat", newlineAndFormat));
}

export function deactivate() {
}
